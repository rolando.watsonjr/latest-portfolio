import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'aos/dist/aos.css'
import Aos from 'aos'



function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
