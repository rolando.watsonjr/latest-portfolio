import {Container, Col, Row, Accordion, Card} from 'react-bootstrap'
import styles from '../styles/AboutMe.module.css'
import Aos from 'aos'
import {useEffect} from 'react'
import Button from '@material-ui/core/Button';

export default function AboutMe(){

	useEffect(()=>{
		Aos.init({duration: 3000})
	}, [])
	return (
		<div id="about" className={styles.WaveBg}>
		<Container>
		<h1 className={styles.AboutMe}>About Me</h1>
		<Row>
		<Col xs={12} md={6} className={styles.Grow}>
		<h1 className={styles.AboutMe}><img data-aos="fade-up" src="/me.jpg" alt="profilepicture" className={styles.ProfilePic}/></h1>
		</Col>
		<Col xs={12} md={6}>
			<Accordion defaultActiveKey="0" className={styles.DropTitle} data-aos="fade-up">
				  <Card>
				    <Card.Header>
				      <Accordion.Toggle as={Button} variant="link" eventKey="0">
				        <Button variant="contained" color="primary">
     						 About Me
 				   				</Button>
				      </Accordion.Toggle>
				    </Card.Header>
				    <Accordion.Collapse eventKey="0">
				      <Card.Body className={styles.DropInfo}>A passionate Full Stack Web Developer. Graduated at Zuitt Coding Bootcamp on the 3rd of November 2020 and acquired skills such as HTML, CSS, Bootstrap, <strong>Javascript and MERN stack</strong>. During my freetime, I am practicing my coding skills and searching for new technologies to gain more knowledge and to acquire new techniques. I will strive to become a professional on this field to create websites and apps that would be beneficial to people.</Card.Body>
				    </Accordion.Collapse>
				  </Card>
				  <Card>
				    <Card.Header>
				      <Accordion.Toggle as={Button} variant="link" eventKey="1">
				        <Button variant="contained" color="primary">
     						 Careers
 				   				</Button>
				      </Accordion.Toggle>
				    </Card.Header>
				    <Accordion.Collapse eventKey="1">
				      <Card.Body className={styles.DropInfo}>A career shifter from real estate to software engineering industry. 5 years total of experience in real estate industry and had only two jobs. The first one was a Billing and Collection Officer in a real estate developer named C5 Mansions Development Corporation located near Bonifacio Global City. The second one was a Team Leader in customer relations department from <strong>one of the leading</strong> Real Estate Developer <strong> in the Philippines</strong> which is Megaworld Corporation.</Card.Body>
				    </Accordion.Collapse>
				  </Card>
				  <Card>
				    <Card.Header>
				      <Accordion.Toggle as={Button} variant="link" eventKey="2">
				        <Button variant="contained" color="primary">
     						 Influences
 				   				</Button>
				      </Accordion.Toggle>
				    </Card.Header>
				    <Accordion.Collapse eventKey="2">
				      <Card.Body className={styles.DropInfo}>Web developer has been my dream job since I was young. I grew up being interested to the technologies as I am a computer game lover and I really would love to take IT course back in University, however, there is no slot available for me that's why I changed my course to business administration instead.</Card.Body>
				    </Accordion.Collapse>
				  </Card>
				</Accordion>	

		</Col>

		</Row>

		</Container>
		</div>
		)
}
