import React, {useEffect} from 'react'
import {Container, Row, Col, Card} from 'react-bootstrap'
import styles from '../styles/Contact.module.css'
import { FaGitlab, FaFacebook, FaLinkedin, FaEnvelope } from 'react-icons/fa';
import Aos from 'aos'





export default function Contact(){
	useEffect(()=>{
		Aos.init({duration: 3000})
	}, [])
	return(
		<div id="contact">
			<Container>
				<h1 className={styles.ContactTitle} data-aos="zoom-in">Contact Me</h1>
				<h5 className={styles.ContactSubTitle} data-aos="zoom-in">Connect with me at these following Web Apps and Email.</h5>
					
						<div className={styles.IconStyle} data-aos="flip-left"
     data-aos-easing="ease-out-cubic">
						    <a href="https://www.linkedin.com/in/ronwatsonjr100/" target="_blank">
						    	<FaLinkedin size="5vh" color="#0077B5" title="Linkedin"/></a>
						    
						    <a href="mailto:rlwatsonjr24@gmail.com">
						    	<FaEnvelope size="5vh" color="blue" title="Email me"/></a>
						    
						    <a href="https://gitlab.com/rolando.watsonjr" target="_blank">
						   	 	<FaGitlab size="5vh" color="#FC6D27"/></a>
						    
						    <a href="https://web.facebook.com/ronwatson24/" target="_blank">
						    	<FaFacebook size="5vh" color="#3b5998"/></a>
						
						</div>
			</Container>
		</div>
		)
}