import {Col, Container, Row} from 'react-bootstrap'
import styles from '../styles/Footer.module.css'

export default function Footer() {
	return (
			<div className={styles.FooterBg}>
				<Container>
					
					<p className={styles.Footer}><img src="logo.svg" width="50"
                height="50"/> Designed and developed by Rolando Watson Jr &copy;</p>
				
				</Container>
			</div>
		)
}