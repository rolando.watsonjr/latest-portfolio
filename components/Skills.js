import {Container, Row, Col, Card, ListGroup} from 'react-bootstrap'
import styles from '../styles/Skills.module.css'
import Typewriter from 'typewriter-effect';
import Aos from 'aos'
import {useEffect} from 'react'


export default function skills(){
	useEffect(()=>{
		Aos.init({duration: 3000})
	}, [])
	return (
	<div id="skills">
		<Container>
		<h1 className={styles.Skills}>Tech Skills</h1>
			<Row>
				
				<Col className={`col d-flex justify-content-center ${styles.Cards}`} data-aos="fade-up">
					<Card style={{ width: '18rem' }} className={styles.CardBorder}>
					  <ListGroup variant="flush" className={styles.CardLogo} >		
					    <a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML" target="_blank"><Card.Img variant="top" src="/html.png" /></a>
							<Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['HTML'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>
					    <a href="https://developer.mozilla.org/en-US/docs/Glossary/CSS" target="_blank"><Card.Img variant="top" src="/css3.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['CSS'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>					    <a href="https://getbootstrap.com/" target="_blank"><Card.Img variant="top" src="/bootstrap.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['BOOTSTRAP'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>					    <a href="https://jquery.com/" target="_blank"><Card.Img variant="top" src="/jquery.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['JQUERY'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>					    
					  </ListGroup>
					</Card>
				</Col>

			
				<Col className={`col d-flex justify-content-center ${styles.Cards}`} data-aos="fade-up">
					<Card style={{ width: '18rem' }} className={styles.CardBorder}>
					  <ListGroup variant="flush" className={styles.CardLogo}>
					    <a href="https://www.mongodb.com/" target="_blank"><Card.Img variant="top" src="/mongodb.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['MONGODB'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>
					   <a href="https://expressjs.com/" target="_blank"><Card.Img variant="top" src="/express.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['EXPRESS JS'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>
					    <a href="https://nextjs.org/" target="_blank"><Card.Img variant="top" src="/nextjs.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['NEXT JS'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>
					   <a href="https://reactjs.org/" target="_blank"><Card.Img variant="top" src="/react.png" /></a>
					    <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['REACT JS'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>
					    
					  </ListGroup>
					</Card>
				</Col>
					
				<Col className={`col d-flex justify-content-center ${styles.Cards}`} data-aos="fade-up">
					<Card style={{ width: '18rem' }} className={styles.CardBorder}>
					  <ListGroup variant="flush" className={styles.CardLogo}>
					 <a href="https://www.javascript.com/" target="_blank"><Card.Img variant="top" src="/javascript.png" /></a>
					  <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['JAVASCRIPT'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>					 <a href="https://nodejs.org/en/" target="_blank"><Card.Img variant="top" src="/nodejs.png" /></a>
					  <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['NODE JS'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>
					<a href="https://about.gitlab.com/" target="_blank"><Card.Img variant="top" src="/gitlab.png" /></a>
					  <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['GITLAB'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>					<a href="https://git-scm.com/" target="_blank"><Card.Img variant="top" src="/git.png" /></a>
					  <Typewriter
							  className={styles.TypeText}
							  options={{
							    strings: ['GIT'],
							    autoStart: true,
							    loop: true,
							    delay: 120
							  }}
							/>					  
					  </ListGroup>
					</Card>
				</Col>

			</Row>
		</Container>
	</div>
		)
}