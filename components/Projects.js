import {Container, Row, Col, Accordion, Card, Button ,ListGroup} from 'react-bootstrap'
import styles from '../styles/Projects.module.css'
import {
  Timeline,
  Events,
  UrlButton,
  ImageEvent,
  TextEvent,
  YouTubeEvent,
} from '@merc/react-timeline';




export default function Projects(){


	return(
		<div id="projects" className={styles.WaveBg}>
			<Container>
				<h1 className={styles.ProjectTitle}>Projects</h1>
				<Timeline>
			      <Events>
			        <ImageEvent
			          date="September 14, 2020"
			          text="1ST CAPSTONE PROJECT / PORTFOLIO MAKING"
			          src="/project1.png"
			          alt="my 1st porfolio"
			          credit="&copy; by Rolando Watson Jr"
			          className={styles.ProjectText}
			        >
			          <div>
			            <Accordion>
						  <Card>
						    
						      <Accordion.Toggle as={Button} variant="link" eventKey="0"  className={styles.ProjHeader}>
						        PROJECT DETAILS
						      </Accordion.Toggle>
						    
						    <Accordion.Collapse eventKey="0">
						      <Card.Body className={styles.ProjectDesc}><strong>Description:</strong> This is my 1st capstone at zuitt bootcamp. This portfolio was made using HTML, CSS and Bootstrap
							      <ListGroup variant="flush">
								    <ListGroup.Item></ListGroup.Item>
								    <ListGroup.Item><strong>Features/Function</strong><br/>
								    <ul>
								    	<li>View my project</li>
								    	<li>Google map integration</li>
								    </ul></ListGroup.Item>
								    <ListGroup.Item><strong>Tech Used</strong> <br/>
								    <ul>
								    	<li>HTML</li>
								    	<li>CSS</li>
								    	<li>BOOTSTRAP</li>
								    	<li>HOSTED VIA VERCEL</li>
								    </ul>
								    
								    </ListGroup.Item>
								  </ListGroup>

							  </Card.Body>
						      
						    </Accordion.Collapse>
						  </Card>
						</Accordion>
						<a href="https://www.youtube.com/watch?v=kQ_6ey0K3lw" target="_blank"><p>Click here to see demo of the project!</p></a>
			          </div>
			        </ImageEvent>
			 
			        <ImageEvent
			          date="September 30, 2020"
			          text="2ND CAPSTONE PROJECT / LOGIN/REGISTRATION WEB APPLICATION"
			          src="/project2.png"
			          alt="my 2nd project"
			          credit="&copy; by Rolando Watson Jr"
			          className={styles.ProjectText}
			        >
			          <div>
			            <Accordion>
						  <Card>
						    
						      <Accordion.Toggle as={Button} variant="link" eventKey="0" className={styles.ProjHeader}>
						        PROJECT DETAILS
						      </Accordion.Toggle>
						    
						    <Accordion.Collapse eventKey="0">
						      <Card.Body className={styles.ProjectDesc}><strong>Description:</strong> This is my 2nd capstone at zuitt bootcamp. You can register and login using this web application. This were made up using backend and front end application.
							      <ListGroup variant="flush">
								    <ListGroup.Item></ListGroup.Item>
								    <ListGroup.Item><strong>Features/Function</strong><br/>
								    <ul>
								    	<li>User Registration</li>
								    	<li>User Login</li>
								    	<li>User Page</li>
								    </ul></ListGroup.Item>
								    <ListGroup.Item><strong>Tech Used</strong> <br/>
								    <ul>
								    	<li>HTML</li>
								    	<li>CSS</li>
								    	<li>BOOTSTRAP</li>
								    	<li>MONGODB</li>
								    	<li>NODE JS</li>
								    </ul>
								    
								    </ListGroup.Item>
								  </ListGroup>

							  </Card.Body>
						      
						    </Accordion.Collapse>
						  </Card>
						</Accordion>
						<a href="https://www.youtube.com/watch?v=utWOn0QznIA" target="_blank"><p>Click here to see demo of the project!</p></a>
			          </div>
			        </ImageEvent>

			        <ImageEvent
			          date="November 3, 2020"
			          text="LAST CAPSTONE/ BUDGET TRACKER WEB APPLICATION"
			          src="/project3.png"
			          alt="jellyfish swimming"
			          credit="&copy; by Rolando Watson Jr"
			          className={styles.ProjectText}
			        >
			          <div>
			            <Accordion>
						  <Card>
						    
						      <Accordion.Toggle as={Button} variant="link" eventKey="0"  className={styles.ProjHeader}>
						        PROJECT DETAILS
						      </Accordion.Toggle>
						    
						    <Accordion.Collapse eventKey="0">
						      <Card.Body className={styles.ProjectDesc}><strong>Description:</strong> This is my last capstone at zuitt bootcamp. Anyone can use this app to monitor their cash flow and expenses.
							      <ListGroup variant="flush">
								    <ListGroup.Item></ListGroup.Item>
								    <ListGroup.Item><strong>Features/Function</strong><br/>
								    <ul>
								    	<li>User Registration/Login</li>
								    	<li>User can login using google account</li>
								    	<li>Can add transaction and see overview in a graph</li>
								    </ul></ListGroup.Item>
								    <ListGroup.Item><strong>Tech Used</strong> <br/>
								    <ul>
								    	<li>HTML</li>
								    	<li>CSS</li>
								    	<li>REACT BOOTSTRAP</li>
								    	<li>NEXT JS</li>
								    	<li>REACT JS</li>
								    	<li>HEROKU</li>
								    	<li>MONGO ATLAS</li>
								    	<li>GITLAB</li>
								    	<li>VERCEL</li>
								    </ul>
								    
								    </ListGroup.Item>
								  </ListGroup>

							  </Card.Body>
						      
						    </Accordion.Collapse>
						  </Card>
						</Accordion>
						<a href="https://www.youtube.com/watch?v=gyNK6p04eyk" target="_blank"><p>Click here to see demo of the project!</p></a>
			          </div>
			        </ImageEvent>
			 		
			<ImageEvent
			          date="November 9, 2020"
			          text="MY LASTEST PORTFOLIO"
			          src="/latestport.png"
			          alt="latestportfolio"
			          credit="&copy; by Rolando Watson Jr"
			          className={styles.ProjectText}
			        >
			          <div>
			            <Accordion>
						  <Card>
						    
						      <Accordion.Toggle as={Button} variant="link" eventKey="0"  className={styles.ProjHeader}>
						        PROJECT DETAILS
						      </Accordion.Toggle>
						    
						    <Accordion.Collapse eventKey="0">
						      <Card.Body className={styles.ProjectDesc}><strong>Description:</strong> This is my latest portfolio up to date. This portfolio was made using ReactJS/NextJS.
							      <ListGroup variant="flush">
								    <ListGroup.Item></ListGroup.Item>
								    <ListGroup.Item><strong>Features/Function</strong><br/>
								    <ul>
								    	<li>View my portfolio/projects</li>
								    	<li>Mobile Responsive</li>
								    </ul></ListGroup.Item>
								    <ListGroup.Item><strong>Tech Used</strong> <br/>
								    <ul>
								    	<li>HTML</li>
								    	<li>CSS</li>
								    	<li>REACT BOOTSTRAP</li>
								    	<li>NEXT JS</li>
								    	<li>REACT JS</li>
								    	<li>MATERIAL UI</li>
								    	<li>GITLAB</li>
								    	<li>VERCEL</li>
								    </ul>
								    
								    </ListGroup.Item>
								  </ListGroup>

							  </Card.Body>
						      
						    </Accordion.Collapse>
						  </Card>
						</Accordion>
						<a href="https://gitlab.com/rolando.watsonjr/latest-portfolio" target="_blank"><p>Click here to view source code!</p></a>
			          </div>
			        </ImageEvent>        

			      </Events>
			    </Timeline>
			</Container>
		</div>
		)
}