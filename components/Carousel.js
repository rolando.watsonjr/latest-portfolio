import React from "react";
import Carousel from "react-bootstrap/Carousel";
import styles from '../styles/Carousel.module.css'

export default function MyCarousal() {
  return (
    <div id="home">
      <Carousel controls={false} indicators interval={2500} pause={false}>
        <Carousel.Item>
          <img className={`d-block w-100 ${styles.CarouselStyle}`} src="/bg.jpg" alt="First slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className={`d-block w-100 ${styles.CarouselStyle}`} src="/bg2.jpg" alt="Second Slide" />
        </Carousel.Item>
        <Carousel.Item>
          <img className={`d-block w-100 ${styles.CarouselStyle}`} src="/bg3.jpg" alt="Third slide" />
        </Carousel.Item>
      </Carousel>
      
    </div>
  );
};