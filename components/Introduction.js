import Typewriter from 'typewriter-effect';
import styles from '../styles/Introduction.module.css'

export default function Introduction(){
	return(
		<div className={styles.IntroStyle}>
		<h1 className={styles.GreetText}>Hello!</h1>
		<h1 className={styles.NameText}><strong>I am Rolando Watson Jr</strong></h1>
		<div className={styles.TypeText}>
		<Typewriter
			  className={styles.TypeText}
			  options={{
			    strings: ['Web Developer', 'Passionate','Perseverance' ],
			    autoStart: true,
			    loop: true,
			    delay: 40
			  }}
			/>
		</div>
		</div>
		)
}