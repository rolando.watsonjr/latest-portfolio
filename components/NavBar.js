import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap'
import styles from '../styles/NavBar.module.css'

export default function NavBar() {
  return (
    <div>
        <Navbar className={styles.NavTheme} variant="dark"expand="lg" fixed="top">
          <Navbar.Brand href="#home">
              <img
                alt=""
                src="/logo.svg"
                width="50"
                height="50"
                className="d-inline-block align-top"
              />{' '}
            </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#about">About</Nav.Link>
              <Nav.Link href="#skills">Skills</Nav.Link>
              <Nav.Link href="#projects">Projects</Nav.Link>
              <Nav.Link href="#contact">Contact</Nav.Link>

            </Nav>
            
          </Navbar.Collapse>
        </Navbar>
     </div>
    )
}

